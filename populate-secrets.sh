#!/bin/sh

work_dir=$(dirname $0)

echo 'DATABASE_URL=postgresql://funkwhale:'$(pass perso/Servers/Funkwhale/pgsql)'@postgres14.postgresql:5432/funkwhale' >$work_dir/secrets.env
echo 'CACHE_URL=redis://:'$(pass perso/Servers/Redis/password)'@redis.default:6379/0' >>$work_dir/secrets.env
echo 'DJANGO_SECRET_KEY='$(pass perso/Servers/Funkwhale/django_secret) >>$work_dir/secrets.env
echo 'AWS_ACCESS_KEY_ID='$(pass perso/Servers/Wasabi/Funkwhale/s3_access_key) >>$work_dir/secrets.env
echo 'AWS_SECRET_ACCESS_KEY='$(pass perso/Servers/Wasabi/Funkwhale/s3_secret_key) >>$work_dir/secrets.env
